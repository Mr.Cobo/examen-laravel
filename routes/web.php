<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PintoresController@getIndex');

Route::get('pintores', 'PintoresController@getIndex');

Route::get('pintores/mostrar/{id}','PintoresController@getVer');

Route::get('cuadros/crear', 'CuadrosController@getCrear');
Route::post('cuadros/crear', 'CuadrosController@postCrear');