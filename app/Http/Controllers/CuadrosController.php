<?php
namespace App\Http\Controllers;

use App\Cuadro;
use App\Pintor;
use Illuminate\Http\Request;

class CuadrosController extends Controller
{
    public function getCrear()
    {
    	return view('cuadros.crear');
    }

    public function postCrear(Request $request)
    {
    	$nombre = $request->nombre;
    	$nombrePintor = $request->pintor;
        $imagen = $request->imagen;        
        //CUADRO OBJECT
        $cuadro = new Cuadro();
        if(isset(Pintor::where('nombre',$nombrePintor)
        ->pluck('id')->all()[0]))
        {
            $idPintor = Pintor::where('nombre',$nombrePintor)
            ->pluck('id')->all()[0];
        }
        else
            return redirect('/');
        $cuadro->nombre=$nombre;
        $cuadro->pintor_id=$idPintor;        

        if(!empty($request->imagen && $request->imagen->isValid()))	
		{			
			$cuadro->imagen = $request->imagen->store('', 'cuadros');		
		}

        try
    	{
    		if($cuadro->save())
	    	{    	
	    		return redirect('/');
	    	}
    	}
    	catch(\Illuminate\Database\QueryException $ex)
    	{
    		return redirect('/');
    	}
    }
}
