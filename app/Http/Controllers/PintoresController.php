<?php

namespace App\Http\Controllers;

use App\Pintor;
use Illuminate\Http\Request;

class PintoresController extends Controller
{
    public function getIndex()
    {
    	return view('pintores.index',array('pintores'=>Pintor::all()));
    }

    public function getVer($id)
    {
    	$pintor = Pintor::findOrFail($id);
    	return view('pintores.mostrar', array('pintor' => $pintor));
    }
}
