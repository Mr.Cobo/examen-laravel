<?php

namespace App;

use App\Cuadro;
use Illuminate\Database\Eloquent\Model;

class Pintor extends Model
{
    protected $table="pintores";

    public function countCuadros($idPintor)
    {
    	return Cuadro::where('pintor_id',$idPintor)->count();    	
    }

    public function cuadros()
    {
    	return $this->hasMany(Cuadro::class);
    }
}
