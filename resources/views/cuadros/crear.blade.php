@extends('layouts.master')
@section('titulo')
Crear cuadro
@endsection
@section('contenido')
  <div class="row">
  	<div class="offset-md-3 col-md-6">
  		<div class="card">
  			<div class="card-header text-center">
  				Añadir nuevo cuadro
  			</div>
  			<div class="card-body" style="padding:30px">
          <!--FORMULARIO-->
  			<form method="POST" action="{{ url('cuadros/crear') }}" enctype="multipart/form-data">
  					{{ csrf_field() }}
            <!--nombre-->
  				<div class="form-group">
  						<label>Nombre</label>
  						<input type="text" name="nombre" class="form-control">
  					</div>
            <!--PINTOR-->
  			<div class="form-group">
              <label>Pintor</label>
              @inject('pintor', 'App\Pintor')
  						<select name="pintor" class="form-control">
                @foreach ($pintor::all() as $pintor)
                {
                    <option value="{{ $pintor->nombre }}">{{ $pintor->nombre }}</option>
                }
                @endforeach
              </select>
  			</div>
            <!--IMAGEN CUADRO-->
  					<div class="form-group">
              <label>Imagen</label>
  						<input type="file" name="imagen" class="form-control">
  					</div>
  					<div class="form-group text-center">
  						<button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
  							Añadir Cuadro
  						</button>
  					</div>
  			</form>
  			</div>
  		</div>
  	</div>
  </div>
@endsection