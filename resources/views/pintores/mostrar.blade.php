@extends('layouts.master')
@section('titulo')
Mostrando
@endsection
@section('contenido')
	<h1>{{$pintor->nombre}}</h1>
	<h2>Pais: {{$pintor->pais}}</h2>
	<h2>Cuadros:</h2>
	<div class="row">
	@foreach($pintor->cuadros as $cuadro)
		<div class="col-xs-12col-sm-6col-md-4">
			<p>{{$cuadro->nombre}}</p>
			<img src="{{url('assets/cuadros')}}/{{$cuadro->imagen}}" width="200" height="200" style="margin:5px">		
		</div>
	@endforeach
	</div>
@endsection