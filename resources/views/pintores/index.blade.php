@extends('layouts.master')
@section('titulo')
INDEX
@endsection
@section('contenido')
<div class='table-responsive'>
	<table class='table table-striped table-sm'>
		<thead>
			<tr>
				<td>Nombre</td><td>Pais</td><td>Cuadros</td>
			</tr>
		</thead>
		<tbody>
		@foreach($pintores as $pintor)
			<tr>
				<td><a href="{{url('pintores/mostrar')}}/{{$pintor->id}}">{{$pintor->nombre}}</a></td>
				<td>{{$pintor->pais}}</td>
				<td>{{$pintor->countCuadros($pintor->id)}}</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>
@endsection